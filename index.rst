.. |label| replace:: Detailseiten-Tabs
.. |snippet| replace:: FvNewTabs
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.7.7
.. |version| replace:: 1.3.3
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Mit diesem Plugin lassen sich zusätzliche Tabreiter auf der Artikeldetailseite anzeigen.
Dadurch kann der Shopbetreiber weitere Informationen (Videos, Soundfiles, Downloads…) zu seinen Artikel hinterlegen. Über das Backend lässt sich konfigurieren, wie die Tabreiter heißen und was jeweils in den Tabs angezeigt werden sollen. Somit ist auch ein kombinierter Tab aus Videos und Sounds möglich. Es ist auch möglich Programmiercode in die Tabreiter einzuspielen, dadurch kann so gut wie alles in ein Tab eingebunden werden.


Frontend
--------
Auf der Artikeldetailseite werden die angelegten Tabs eingeblendet wenn bei dem Artikel ein Wert existiert.

.. image:: FvNewTabs5.png

.. image:: FvNewTabs6.png


Backend
-------

.. image:: FvNewTabs1.png

Hier können Sie das Plugin konfigurieren.

.. image:: FvNewTabs2.png

Hier legen Sie die neue Tabs an.

:Tab hinzufügen: Hier können Sie einen weiteren Tab anlegen.

.. image:: FvNewTabs3.png

Hier legen Sie die Elemente an die in den Tab angezeigt werden sollen.

:Elemente hinzufügen: Hier können Sie Elemnte anlegen die in den Tab angezeigt werden sollen.
:Tab löschen: Hier können Sie den Tab löschen.
:Tab speichern: Hier wird die Konfiguration des Tabs gespeichert.

:Tab-Name: Dies wird der Titel des Tabs.
:Position: Hier können Sie die Sortierung der Tabs beeinflussen.
:Beschreibung: Hier geben Sie je nach Wunsch eine interne Beschreibung an (wird nicht im Frontend angezeigt).
:Hinter Rating: Markieren Sie dies wenn Sie den Tab Hinter der Bewertung angezeigt haben wollen.
:Aktiv: Hier aktivieren Sie den Tab.

.. image:: FvNewTabs4.png

Hier weißen Sie die Freitextfelder den Elementen zu.

:Element löschen: Hier können Sie das Element löschen.
:Element speichern: Hier wird der Konfiguration des Elementes gespeichert.

:Element-Name: Hier können Sie dem Element einen Name geben.
:Position: Hier können Sie die Sortierung des Elementes beeinflussen.
:Typ: Hier wählen Sie die Art der Verknüfung aus.
:Verknüpfung: Hier wählen Sie das Freitext-Feld wo die Information des anzuzeigenden Wert beim Artikel hinterlegt ist.
:User Attribut um Element anzuzeigen: Wenn das ausgewählte Freitextfeld(Kunde) aktiv ist dann wird die Blockierung aufgehoben und das Element wird angezeigt, ansonsten ist es Unsichtbar.
:Erweiterung: Diese Erweiterung ersetzt den Wert der Verknüfung. Style oder konstanter Text lässt sich damit im Element einbetten. Ebenfalls hat man Zugriff auf alle Smartyvariablen, die auf der Artikeldetailseite (auch Kundendaten können als smarty-Variable ausgelesen werden) verwendet werden. zB. {$sArticle.attr3},{$sArticle.ordernumber}, {$sCategoryCurrent}, {$sShopname} ... ACHTUNG! "müssen mit ' ersetzt werden. Anwendungsbeispiel <p style='color: red'>{$sArticle.attr3}</p>.
:Aktiv: Hier können Sie das Element aktiv oder inaktiv schalten.

bei download

:Pfad: Dies wird nur eingeblendet bei Typ download. Hier hinterlegen Sie den Pfad wo die Datein liegen.
:Ersetzungen: Dies wird nur eingeblendet bei typ download. Hier können Sie Zeichen ersetzen. Im Standard werden "_" zu Leerzeichen.

bei link

:Benennung des Links: Hier kann ein weiteres Freitextfeld ausgewählt werden, was im Frontend als Label dient.
:Shopware Icon: Hier können Sie eine Icon-Klasse hinterlegen um im frontend ein Icon anzeigen zu lassen.

bei shopware

:shopware Templates: Hier können Sie die Eigenschaften, Kommentare, Product Stream, Zubehör und Ähnliche Artikel zuweisen.

technische Beschreibung
------------------------
Typen der Elemente
__________________

:code: Wählen Sie dies wenn in dem Freitext-Feld ein Text steht. Hier wird ohne Erweiterung der Inhalt des Feldes dargestelt. Nach wunsch kann ein Snippet bei Erweiterung hinterlegt werden.
:download: Wählen Sie dies wenn in dem Freitext-Feld die Datei hinterlegt ist. Es muss der Pfad mit angegeben werden wo die Datein hinterlegt sind.
:link: Wählen Sie dies wenn Sie einen Hyperlink hinterlegen wollen.
:shopware: Wählen Sie dies wenn Sie Shopware-Template in den Element hinzufügen wollen.
:soundcloud: Wählen Sie dies wenn in dem Freitext-Feld der Link zu einem SoundCloud-Link hinterlegt ist.
:vimeo: Wählen Sie dies wenn in dem Freitext-Feld der Link zu einem Vimeo-Video hinterlegt ist.
:youtube: Wählen Sie dies wenn in dem Freitext-Feld der Link zu einem YouTube-Video hinterlegt ist.

Modifizierte Template-Dateien
-----------------------------
:/details/tabs.tpl:
:/details/tabs_inner.tpl:



